import 'package:meta/meta.dart';
import 'package:injectable/injectable.dart';
import 'package:tech_12/common/models/use_case.dart';
import 'package:tech_12/domain/entities/candidate_entity.dart';
import 'package:tech_12/domain/repository/candidate_repository.dart';

@lazySingleton
@injectable
class GetCandidateUsecase
    implements UseCase<List<CandidateEntity>, Map<String, String>>{
  final CandidateRepository candidateRepository;

  GetCandidateUsecase({@required this.candidateRepository});

  @override
  Future<List<CandidateEntity>> call(Map payload) {
   return candidateRepository.getAllCandidate();
  }

}