import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class CandidateEntity extends Equatable {
  final String id;
  final String name;
  final String email;
  final String phoneNumber;
  final String study;

  CandidateEntity(
      {@required this.id,
      @required this.name,
      @required this.email,
      @required this.phoneNumber,
      @required this.study});

  List<Object> get props => [id, name, email, phoneNumber, study];

  CandidateEntity copyWith(
          {String id,
          String name,
          String email,
          String phoneNumber,
          String study}) =>
      CandidateEntity(
          id: id ?? this.id,
          name: name ?? this.name,
          email: email ?? this.email,
          phoneNumber: phoneNumber ?? this.phoneNumber,
          study: study ?? this.study);
}
