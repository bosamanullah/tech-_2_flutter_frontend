import 'package:injectable/injectable.dart';
import 'package:tech_12/domain/entities/candidate_entity.dart';
import 'package:tech_12/data/repository/candidate_repository_impl.dart';

@Bind.toType(CandidateRepositoryImpl)
@injectable
abstract class CandidateRepository {
  Future<List<CandidateEntity>> getAllCandidate();
}