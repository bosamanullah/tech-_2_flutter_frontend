import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:injectable/injectable.dart';
import 'package:tech_12/data/models/candidate_model.dart';

@Bind.toType(CandidateRemoteDataSourceImpl)
@injectable
abstract class CandidateDataSource {
  Future<List<Candidate>> getAllCandidate();
}
@lazySingleton
@injectable
class CandidateRemoteDataSourceImpl implements CandidateDataSource {
  final Dio httpClient;


  CandidateRemoteDataSourceImpl({@required this.httpClient});

  @override
  Future<List<Candidate>> getAllCandidate() async {
    List<Candidate> candidates = [];
    final Response response = await httpClient.get('https://2aymyuqnj5.execute-api.ap-southeast-1.amazonaws.com/dev/candidates');
    print("1 response");
    print(response);
    final data = response.data as List;
    print("2 data");
    print(data);
    data.asMap().forEach((index, candidate) {
      candidates.add(Candidate.fromJson(index, candidate as Map<String, dynamic>));
    });
    return candidates;
  }
}