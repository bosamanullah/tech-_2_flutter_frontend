import 'package:meta/meta.dart';
import 'package:injectable/injectable.dart';
import 'package:tech_12/data/datasource/candidate_datasource.dart';
import 'package:tech_12/domain/entities/candidate_entity.dart';
import 'package:tech_12/domain/repository/candidate_repository.dart';

@lazySingleton
@injectable
class CandidateRepositoryImpl implements CandidateRepository {
  final CandidateDataSource candidateDataSource;
  CandidateRepositoryImpl({@required this.candidateDataSource});

  @override
  Future<List<CandidateEntity>> getAllCandidate() async {
    final candidate = await candidateDataSource.getAllCandidate();
    return candidate;
  }
}