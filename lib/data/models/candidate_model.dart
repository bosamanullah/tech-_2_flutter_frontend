import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:tech_12/domain/entities/candidate_entity.dart';

class Candidate extends CandidateEntity {
  Candidate({
    @required String id,
    @required String name,
    @required String email,
    @required String phoneNumber,
    @required String study,
  }) : super(
            id: id,
            name: name,
            email: email,
            phoneNumber: phoneNumber,
            study: study);

  factory Candidate.fromJson(int index, Map<String, dynamic> json) {
    return Candidate(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        phoneNumber: json['phoneNUmber'],
        study: json['study']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'phoneNumber': phoneNumber,
      'email': email,
      'study': study
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
