part of 'candidate_bloc.dart';

abstract class CandidateState extends Equatable {
  const CandidateState();
  @override
  List<Object> get props => [];
}

class CandidateInitial extends CandidateState{}

class CandidateLoading extends CandidateState{}

class CandidateLoaded extends CandidateState{
  final List<Candidate> candidate;
  CandidateLoaded({@required this.candidate});
  CandidateLoaded copyWith({List<Candidate> candidate}) {
    return CandidateLoaded(candidate: candidate ?? this.candidate);
  }

  @override
  List<Object> get props => [candidate];

  @override
  String toString() => '$candidate';
}

class CandidateError extends CandidateState {}