part of 'candidate_bloc.dart';

abstract class CandidateEvent extends Equatable {
  const CandidateEvent();
}

class GetCandidate extends CandidateEvent {
  @override
  List<Object> get props => [];
}