import 'dart:async';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:equatable/equatable.dart';
import 'package:tech_12/domain/usecase/candidate_usecase.dart';
import 'package:tech_12/data/models/candidate_model.dart';

part 'candidate_event.dart';
part 'candidate_state.dart';

@injectable
class CandidateBloc extends Bloc<CandidateEvent, CandidateState> {
  final GetCandidateUsecase getCandidateUsecase;

  CandidateBloc({@required this.getCandidateUsecase});

  @override
  CandidateState get initialState => CandidateInitial();

  @override
  Stream<CandidateState> mapEventToState(CandidateEvent event) async* {
    if (event is GetCandidate) {
      print("###############################################");
      print(event);
      print("###############################################");
      //yield CandidateLoading();
      try {
        if (state is CandidateInitial) {
          Map payload = {};
          final candidateResult = await getCandidateUsecase.call(payload);
          print("=========================================================");
          //print(candidateResult);
          print("=========================================================");
          yield CandidateLoaded(candidate: candidateResult);
        }
      } catch (err) {
        yield CandidateError();
      }
    }
  }
}
