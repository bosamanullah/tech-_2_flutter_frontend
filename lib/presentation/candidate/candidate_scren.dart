import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tech_12/presentation/candidate/bloc/candidate_bloc.dart';

class CandidateScreen extends StatefulWidget {
  @override
  _CandidateScreenState createState() => _CandidateScreenState();
}

class _CandidateScreenState extends State<CandidateScreen> with TickerProviderStateMixin {
  CandidateBloc candidateBloc;

  @override
  void initState() {
    super.initState();
    candidateBloc = BlocProvider.of<CandidateBloc>(context);

    if (mounted) {
      candidateBloc.add(
        GetCandidate(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("widget title")),
      body: Center(
        child: Container(
          child: ListView(
            children: <Widget>[
              Text("ini screen nya"),
              BlocBuilder<CandidateBloc, CandidateState>(
                builder: (context, state) {
                  print("*************************************");
                  print(state);
                  print("*************************************");
                  if (state is CandidateInitial) {}
                  if (state is CandidateLoaded) {
                    print("---------------------------------------");
                    print(state);
                    print("---------------------------------------");
                    return Container(
                        color: Colors.pink,
                        height: 100,
                        width: 100,
                        child: ListView.builder(
                            itemCount: state.candidate.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  Text(state.candidate[index].name),
                                  Text(state.candidate[index].id),
                                ],
                              );
                            }));
                  }
                  if (state is CandidateError) {
                    return Container(
                      child: Text("Kayaknya eroor"),
                    );
                  }
                  return Text("semua event hanya lewat");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
