import 'package:flutter/material.dart';
import 'package:tech_12/common/routes/routes.dart';
import 'package:tech_12/common/routes/routing.dart';

void main() => runApp(CandidateApp());

class CandidateApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "RESERVE PROXY",
      initialRoute: Routes.CANDIDATE_SCREEN,
      onGenerateRoute: RouteGenerator.generatorRoutes,
    );
  }

}
