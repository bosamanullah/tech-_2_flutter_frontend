import 'package:flutter/material.dart';
import 'package:tech_12/common/config/injector.dart';
import 'package:tech_12/common/config/log_config.dart';

import 'app.dart' as app;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupInjections();
//  setupLogger();
  app.main();
}