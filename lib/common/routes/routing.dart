import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tech_12/common/config/injector.dart';
import 'package:tech_12/common/routes/routes.dart';
import 'package:tech_12/presentation/candidate/bloc/candidate_bloc.dart';
import 'package:tech_12/presentation/candidate/candidate_scren.dart';
import 'package:tech_12/presentation/error/error_screen.dart';

class RouteGenerator {
  static Route <dynamic> generatorRoutes(RouteSettings settings) {
    switch (settings.name) {
      case Routes.CANDIDATE_SCREEN:
        return MaterialPageRoute(builder: (context) => _buildCandidateScreen());
      default :
        return MaterialPageRoute(builder: (context) => _buildErrorScreen());
    }

  }


  static Widget _buildCandidateScreen() {
    return BlocProvider<CandidateBloc>(
      create: (BuildContext context) => getIt<CandidateBloc>(),
      child: CandidateScreen(),
    );
  }

  static Widget _buildErrorScreen() {
    return ErrorScreen();
  }
}