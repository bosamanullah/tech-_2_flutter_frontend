import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';


@Bind.toType(RestClient)
@injectable
abstract class RestClientBase with DioMixin implements Dio {
  Dio setup();
}

@lazySingleton
@injectable
class RestClient extends RestClientBase {
  Dio client = Dio();

  RestClient() {
    client.options.baseUrl = 'https://2aymyuqnj5.execute-api.ap-southeast-1.amazonaws.com/dev/candidates';
    client.options.headers['content-Type'] = 'application/json';
  }

  @override
  Dio setup() {
    return client;
  }
}