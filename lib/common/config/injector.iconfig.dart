// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:tech_12/data/datasource/candidate_datasource.dart';
import 'package:dio/src/dio.dart';
import 'package:tech_12/data/repository/candidate_repository_impl.dart';
import 'package:tech_12/domain/repository/candidate_repository.dart';
import 'package:tech_12/domain/usecase/candidate_usecase.dart';
import 'package:tech_12/common/networks/rest_client.dart';
import 'package:tech_12/presentation/candidate/bloc/candidate_bloc.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<CandidateDataSource>(
        () => CandidateRemoteDataSourceImpl(httpClient: getIt<Dio>()))
    ..registerLazySingleton<CandidateRemoteDataSourceImpl>(
        () => CandidateRemoteDataSourceImpl(httpClient: getIt<Dio>()))
    ..registerLazySingleton<CandidateRepositoryImpl>(() =>
        CandidateRepositoryImpl(
            candidateDataSource: getIt<CandidateDataSource>()))
    ..registerFactory<CandidateRepository>(() => CandidateRepositoryImpl(
        candidateDataSource: getIt<CandidateDataSource>()))
    ..registerLazySingleton<GetCandidateUsecase>(() =>
        GetCandidateUsecase(candidateRepository: getIt<CandidateRepository>()))
    ..registerFactory<RestClientBase>(() => RestClient())
    ..registerLazySingleton<RestClient>(() => RestClient())
    ..registerFactory<CandidateBloc>(
        () => CandidateBloc(getCandidateUsecase: getIt<GetCandidateUsecase>()));
}
